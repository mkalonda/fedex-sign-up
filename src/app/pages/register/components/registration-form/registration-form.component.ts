import { Component } from '@angular/core';
import { FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { FormValidatorService } from '../../../../shared/helper/form-validator.service';
import { CustomerService } from '../../../../shared/service/customer.service';

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.scss']
})
export class RegistrationFormComponent {

  public isPasswordVisible = false;
  public registrationForm: FormGroup;
  public isSubmitted = false;

  constructor(private formBuilder: FormBuilder,
              private formValidatorService: FormValidatorService,
              private customerService: CustomerService) {
    this.registrationForm = this.formBuilder.group({
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        password: [
          '',
          [
            Validators.min(0),
            Validators.minLength(8),
            Validators.required,
            formValidatorService.hasLowerCase,
            formValidatorService.hasNumber,
            formValidatorService.hasUpperCase,
          ]
        ],
        email: ['', [Validators.required, Validators.email]],
      },
      {
        validators: [
          (formGroup: FormGroup) => this.formValidatorService.formFieldDoesNotContain(formGroup, 'password', ['firstName', 'lastName'])
        ]
      }
    );
  }

  public getFormErrorByFormFieldName(formFieldName: string): ValidationErrors | null {
    const formFieldAbstractControl = this.registrationForm.get(formFieldName);
    return formFieldAbstractControl && formFieldAbstractControl.errors;
  }

  public getInputValueLength(inputValue: string): number {
    return inputValue.length || 0;
  }

  public getPasswordInputType(): string {
    return this.isPasswordVisible ? 'text' : 'password';
  }

  public getPasswordVisibilityState(): string {
    return this.isPasswordVisible ? 'visibility' : 'visibility_off';
  }

  public resetFormSubmition(): void {
    this.isSubmitted = false;
  }

  public async submitForm(): Promise<void> {
    if (this.isFormValid()) {
      await this.customerService.createCustomer(this.registrationForm.value);

      this.isSubmitted = true;
      this.resetRegistrationForm();
    }
  }

  public togglePasswordVisibility(toggleState: boolean): void {
    this.isPasswordVisible = toggleState;
  }

  private resetRegistrationForm(): void {
    this.registrationForm.reset();

    Object.keys(this.registrationForm.controls).forEach(key => {
      this.registrationForm.controls[key].setErrors(null);
    });
  }

  private isFormValid(): boolean {
    return this.registrationForm.valid;
  }
}
