import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrationFormComponent } from './registration-form.component';
import { CustomerService } from '../../../../shared/service/customer.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormValidatorService } from '../../../../shared/helper/form-validator.service';

describe('RegistrationFormComponent', () => {
  let component: RegistrationFormComponent;
  let fixture: ComponentFixture<RegistrationFormComponent>;
  let customerServiceSpy: any;

  beforeEach(async () => {
    customerServiceSpy = jasmine.createSpyObj('CustomerService', ['createCustomer']);
    await TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        FormValidatorService,
        {provide: CustomerService, useValue: customerServiceSpy},
      ],
      declarations: [RegistrationFormComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationFormComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('getInputValueLength', () => {
    it('should return correct length of input value', () => {
      expect(component.getInputValueLength('test')).toBe(4);

    });

    it('should return default input value when no input is set', () => {
      expect(component.getInputValueLength('')).toBe(0);
    });
  });

  describe('getFormErrorByFormFieldName', () => {
    it('should return form errors by field name', () => {
      component.registrationForm.controls.password.setValue('');
      component.getFormErrorByFormFieldName('password');

      expect(component.getFormErrorByFormFieldName('password')).toEqual({
        required: true,
        hasLowerCase: true,
        hasNumber: true,
        hasUpperCase: true
      });
    });
  });

  describe('getPasswordVisibilityState', () => {
    it('should return correct string when password is not visible', () => {
      component.isPasswordVisible = false;
      expect(component.getPasswordVisibilityState()).toBe('visibility_off');
    });

    it('should return correct string when password is visible', () => {
      component.isPasswordVisible = true;
      expect(component.getPasswordVisibilityState()).toBe('visibility');
    });
  });


  describe('getPasswordInputType', () => {
    it('should return password type string when password is not visible', () => {
      component.isPasswordVisible = false;
      expect(component.getPasswordInputType()).toBe('password');
    });

    it('should return text type string when password is visible', () => {
      component.isPasswordVisible = true;
      expect(component.getPasswordInputType()).toBe('text');
    });
  });

  describe('resetFormSubmition', () => {
    it('should set isSubmitted to false', () => {
      component.isSubmitted = true;
      component.resetFormSubmition();

      expect(component.isSubmitted).toBeFalsy();
    });
  });

  describe('submitForm', () => {
    it('should not submit form when form is not valid', () => {
      component.submitForm();
      expect(customerServiceSpy.createCustomer).not.toHaveBeenCalled();
    });

    it('should submit form when form is valid', () => {
      component.registrationForm.controls.email.setValue('madi@fedex.com');
      component.registrationForm.controls.password.setValue('Testing123#');
      component.registrationForm.controls.firstName.setValue('Madi');
      component.registrationForm.controls.lastName.setValue('Kalonda');
      component.submitForm();

      expect(customerServiceSpy.createCustomer).toHaveBeenCalledWith({
        firstName: 'Madi',
        lastName: 'Kalonda',
        password: 'Testing123#',
        email: 'madi@fedex.com'
      });
    });
  });

  describe('togglePasswordVisibility', () => {
    it('should set isPasswordVisible state', () => {
      component.isPasswordVisible = false;
      component.togglePasswordVisibility(true);

      expect(component.isPasswordVisible).toBe(true);
    });
  });
});

