import { Injectable } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})

export class FormValidatorService {

  public formFieldDoesNotContain(formGroup: FormGroup, fieldName: string, formFieldNames: string[]): void | null {
    const selectedFieldName = formGroup.get(fieldName);

    if (selectedFieldName === null) {
      return null;
    }

    for (const formFieldName of formFieldNames) {
      const selectedFormFieldName = formGroup.get(formFieldName);
      const formFieldErrorName = `${fieldName}Contains${formFieldName}`;

      if (selectedFormFieldName === null) {
        return null;
      }

      if (this.selectedfieldNameIncludesFieldValues(selectedFormFieldName.value, selectedFieldName.value)) {
        return selectedFieldName.setErrors({[`${formFieldErrorName}`]: true});
      }

      if (selectedFieldName.hasError(formFieldErrorName)) {
        selectedFieldName.setErrors(null);
        selectedFieldName.updateValueAndValidity();
      }
    }
  }

  public hasLowerCase(control: FormControl): IValidationResult | null {
    const hasLower = /[a-z]/.test(control.value);

    if (!hasLower) {
      return {hasLowerCase: true};
    }

    return null;
  }

  public hasNumber(control: FormControl): IValidationResult | null {
    const hasNumber = /\d/.test(control.value);

    if (!hasNumber) {
      return {hasNumber: true};
    }

    return null;
  }

  public hasUpperCase(control: FormControl): IValidationResult | null {
    const hasUpperCase = /[A-Z]/.test(control.value);

    if (!hasUpperCase) {
      return {hasUpperCase: true};
    }

    return null;
  }

  private selectedfieldNameIncludesFieldValues(selectedFormFieldName: string, selectedFieldName: string): false | boolean {
    return !!selectedFormFieldName
      && selectedFormFieldName.length > 0
      && !!selectedFieldName
      && selectedFieldName.length > 0
      && selectedFieldName.includes(selectedFormFieldName);
  }
}

export interface IValidationResult {
  [key: string]: boolean;
}
