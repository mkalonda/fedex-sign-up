import { TestBed } from '@angular/core/testing';

import { FormValidatorService } from './form-validator.service';
import { FormBuilder, FormControl } from '@angular/forms';

describe('FormValidatorService', () => {
  let sut: FormValidatorService;
  const formBuilder: FormBuilder = new FormBuilder();

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {provide: FormBuilder, useValue: formBuilder}
      ]
    });

    sut = TestBed.inject(FormValidatorService);
  });

  it('should be created', () => {
    expect(sut).toBeTruthy();
  });

  describe('formFieldDoesNotContain', () => {
    it('should return null when form field does not contain selected values', () => {
      const mockedForm = formBuilder.group({
        firstName: 'Madi',
        lastName: 'Kalonda',
        password: 'test123#',
        email: 'madi@fedex.com'
      });

      expect(sut.formFieldDoesNotContain(mockedForm, 'password', ['firstName, lastName'])).toEqual(null);
    });

    it('should remove error from FormControl errors object when error is resolved', () => {
      const mockedForm = formBuilder.group({
        firstName: 'Madi',
        lastName: 'Kalonda',
        password: 'Kalonda',
        email: 'madi@fedex.com'
      });
      const passwordFormControl = mockedForm.get('password');

      sut.formFieldDoesNotContain(mockedForm, 'password', ['firstName', 'lastName']);
      mockedForm.controls.lastName.setValue('Madi');
      sut.formFieldDoesNotContain(mockedForm, 'password', ['firstName', 'lastName']);

      if (passwordFormControl) {
        expect(passwordFormControl.hasError('passwordContainslastName')).toBe(false);
      }
    });

    it('should return error object when form field contains selected values', () => {
      const mockedForm = formBuilder.group({
        firstName: 'Madi',
        lastName: 'Kalonda',
        password: 'Kalonda',
        email: 'madi@fedex.com'
      });
      const passwordFormControl = mockedForm.get('password');

      sut.formFieldDoesNotContain(mockedForm, 'password', ['firstName', 'lastName']);

      if (passwordFormControl) {
        expect(passwordFormControl.hasError('passwordContainslastName')).toBe(true);
      }
    });

    it('should return null when fieldName is not present on the form', () => {
      const mockedForm = formBuilder.group({
        firstName: 'Madi',
        lastName: 'Kalonda',
        password: 'test123#',
        email: 'madi@fedex.com'
      });

      expect(sut.formFieldDoesNotContain(mockedForm, 'passwordd', ['firstName, lastName'])).toEqual(null);
    });
  });

  describe('hasLowerCase', () => {
    it('should return hasLowerCase error object when value has no lowerCase', () => {
      const value = {value: 'TEST'} as FormControl;
      expect(sut.hasLowerCase(value)).toEqual({hasLowerCase: true});
    });

    it('should return null when value has lowerCase', () => {
      const value = {value: 'test'} as FormControl;
      expect(sut.hasLowerCase(value)).toEqual(null);
    });
  });

  describe('hasNumber', () => {
    it('should return hasNumber error object when value has no number', () => {
      const value = {value: 'testing'} as FormControl;
      expect(sut.hasNumber(value)).toEqual({hasNumber: true});
    });

    it('should return null when value has number', () => {
      const value = {value: 'testing123'} as FormControl;
      expect(sut.hasNumber(value)).toEqual(null);
    });
  });

  describe('hasUpperCase', () => {
    it('should return hasNumber error object when value has no UpperCase', () => {
      const value = {value: 'uppercase'} as FormControl;
      expect(sut.hasUpperCase(value)).toEqual({hasUpperCase: true});
    });

    it('should return null when value has upperCase', () => {
      const value = {value: 'Uppercase'} as FormControl;
      expect(sut.hasUpperCase(value)).toEqual(null);
    });
  });
});
