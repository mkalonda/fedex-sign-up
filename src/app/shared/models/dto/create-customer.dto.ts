export interface ICreateCustomerDto {
  firstName: string;
  lastName: string;
  password: string;
  email: string;
}
