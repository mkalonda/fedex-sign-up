import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { ICreateCustomerDto } from '../models/dto/create-customer.dto';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(public http: HttpClient) {
  }

  public createCustomer(customerRegistrationFrom: ICreateCustomerDto): Promise<{}> {
    return this.http.post(`${environment.apiUrl}/users`, customerRegistrationFrom).toPromise();
  }
}
