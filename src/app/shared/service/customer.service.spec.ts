import { TestBed } from '@angular/core/testing';

import { CustomerService } from './customer.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('RequestMapService', () => {
  let sut: CustomerService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CustomerService],
    });

    sut = TestBed.inject(CustomerService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(sut).toBeTruthy();
  });

  describe('createCustomer', () => {

    it('should post ..', () => {
      const createCustomerFormDto = {
        email: 'madi@fedex.com',
        firstName: 'Madi',
        lastName: 'kalonda',
        password: 'Test123#'
      };

      sut.createCustomer(createCustomerFormDto);

      const testRequest = httpMock.expectOne('https://demo-api.now.sh/users');

      expect(testRequest.request.method).toEqual('POST');
      expect(testRequest.request.body).toEqual(createCustomerFormDto);
    });
  });
});
