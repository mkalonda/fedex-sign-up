context('RegisterPage', () => {
  beforeEach(() => {
    cy.server();
    cy.route({
      method: 'POST',
      url: 'https://demo-api.now.sh/users',
    }).as("createCustomerRequest")
    cy.visit('http://localhost:4200')
  });


  it('the registration form should be visible', () => {
    cy.get('[data-test="registrationForm"]').should('exist');
  });

  describe('Form fields', () => {
    describe('FirstName field', () => {
      it('firstName field show required error message when invalid', () => {
        cy.get('[data-test="firstNameField"]')
          .should('exist')
          .click()
          .blur()

        cy.get('mat-error').contains('This field is required')
      });

      it('should not show error message when field is valid', () => {
        cy.get('[data-test="firstNameField"]')
          .should('exist')
          .type('Madi')
          .blur();

        cy.get('mat-error').should('not.exist');
      });

      it('should show required error message when form changes from valid to invalid', () => {
        cy.get('[data-test="firstNameField"]')
          .should('exist')
          .type('Madi');

        cy.get('mat-error').should('not.exist');

        cy.get('[data-test="firstNameField"]')
          .clear()
          .blur();

        cy.get('mat-error').contains('This field is required')
      });
    });

    describe('LastName field', () => {
      it('lastName field show required error message when invalid', () => {
        cy.get('[data-test="lastNameField"]')
          .should('exist')
          .click()
          .blur();

        cy.get('mat-error').contains('This field is required')
      })

      it('should not show error message when field is valid', () => {
        cy.get('[data-test="lastNameField"]')
          .should('exist')
          .type('Kalonda')
          .blur();

        cy.get('mat-error').should('not.exist');
      });

      it('should show required error message when form changes from valid to invalid', () => {
        cy.get('[data-test="lastNameField"]')
          .should('exist')
          .type('Kalonda');

        cy.get('mat-error').should('not.exist');

        cy.get('[data-test="lastNameField"]')
          .clear()
          .blur();

        cy.get('mat-error').contains('This field is required')
      })
    });

    describe('password field', () => {
      it('should show correct error message when invalid', () => {
        cy.get('[data-test="passwordField"]')
          .should('exist')
          .click()
          .blur();

        cy.get('mat-error')
          .should('contain', 'This field is required.')
          .and('contain', 'This field requires minimum length of 8 characters.')
          .and('contain', 'Password requires lowercase.')
          .and('contain', 'Password requires number.')
          .and('contain', 'Password requires uppercase.')
      });

      it('should show required error message when invalid', () => {
        cy.get('[data-test="passwordField"]')
          .should('exist')
          .click()
          .blur();

        cy.get('mat-error').contains('This field is required')
      });

      it('should show minimum length error message when invalid', () => {
        cy.get('[data-test="passwordField"]')
          .should('exist')
          .type('short')
          .blur();

        cy.get('mat-error').contains('This field requires minimum length of 8 characters.')
      });

      it('should show lowercase error message when invalid', () => {
        cy.get('[data-test="passwordField"]')
          .should('exist')
          .type('UPPERCASE')
          .blur();

        cy.get('mat-error').contains('Password requires lowercase.')
      });

      it('should show uppercase error message when invalid', () => {
        cy.get('[data-test="passwordField"]')
          .should('exist')
          .type('lowercase')
          .blur();

        cy.get('mat-error').contains('Password requires uppercase.')
      });


      it('should show number error message when invalid', () => {
        cy.get('[data-test="passwordField"]')
          .should('exist')
          .type('numbers')
          .blur();

        cy.get('mat-error').contains('Password requires number.')
      });

      it('should not show error message when field is valid', () => {
        cy.get('[data-test="passwordField"]')
          .should('exist')
          .type('TheMostSecurePasswordEver123')
          .blur();

        cy.get('mat-error').should('not.exist');
      });
    });
  });

  describe('Form submission', () => {
    it('when pressing submit button it should show error messages when form is invalid', () => {
      cy.get('[data-test="submitButton"]')
        .should('exist')
        .click();

      cy.get('mat-error')
        .should('exist')
        .its('length')
        .should('be.gt', 0)
    });

    it('should show success message when form is submitted', () => {
      cy.get('[data-test="firstNameField"]').type('Madi');
      cy.get('[data-test="lastNameField"]').type('Kalonda');
      cy.get('[data-test="passwordField"]').type('TheMostSecurePasswordEver123');
      cy.get('[data-test="emailField"]').type('madi@fedex.com');
      cy.get('[data-test="submitButton"]')
        .click()
        .wait('@createCustomerRequest')
        .get('@createCustomerRequest')
        .then(xhr => {
          expect(xhr.status).to.eq(200);
          expect(xhr.request.body).to.deep.equal({
            email: "madi@fedex.com",
            firstName: "Madi",
            lastName: "Kalonda",
            password: "TheMostSecurePasswordEver123"
          });
        });
      cy.get('[data-test="formSubmittedText"]').should('exist');
    });
  });
});
